package com.wpp.hundred;

import com.wpp.handred.HundredApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HundredApplication.class)
class HundredApplicationTests {

    @Test
    void contextLoads() {
    }

}
