package com.wpp.handred.auth;


import lombok.Data;

@Data
public class Result {
    private Long code;
    private String msg;


}