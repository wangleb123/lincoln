package com.wpp.handred.auth;


import java.io.Serializable;

public class DataResult<T> extends Result implements Serializable {

    private T result;

    public T getResult() {
        return (T) result;
    }

    public DataResult<T>  setResult(T result) {
        this.result = result;
        return this;
    }
    public DataResult<T>  ok() {
        this.setCode(0L);
        this.setMsg("成功");
        return this;
    }




}