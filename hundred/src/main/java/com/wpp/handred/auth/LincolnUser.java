package com.wpp.handred.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.lexiang.oauth.WLUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LincolnUser implements WLUser {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "微信openId")
    private String openId;

    private String headImg;

    @ApiModelProperty(value = "微信昵称")
    private String nikeName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "姓名全称")
    private String name;

    @ApiModelProperty(value = "状态 0 未激活 1 已启用")
    private String status;

    private String sessionKey;

}
