package com.wpp.handred.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.lexiang.oauth.entity.wx.resp.Code2SessionResp;
import com.lexiang.oauth.entity.wx.resp.PhoneNumberInfoResp;
import com.lexiang.oauth.entity.wx.resp.PhoneNumberResp;
import com.lexiang.oauth.entity.wx.UserInfoReq;
import com.lexiang.oauth.entity.wx.resp.UserInfoResp;
import com.lexiang.oauth.service.LoginService;
import com.lexiang.oauth.service.WxAppService;
import com.lexiang.oauth.utils.BusinessException;
import com.lexiang.oauth.utils.CodeEnum;
import com.lexiang.oauth.utils.WLObjectUtils;
import com.wpp.handred.auth.LincolnUser;
import com.wpp.handred.entity.Enroll;
import com.wpp.handred.entity.LoginInfo;
import com.wpp.handred.entity.User;
import com.wpp.handred.entity.dto.LoginDTO;
import com.wpp.handred.mapper.LoginInfoMapper;
import com.wpp.handred.service.AppService;
import com.wpp.handred.service.IEnrollService;
import com.wpp.handred.service.IUserService;
import com.wpp.handred.utils.WLBeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;

@Service
public class AppServiceImpl implements AppService {


    @Resource
    private WxAppService wxAppService;

    @Resource
    private LoginService loginService;

    @Resource
    private IUserService userService;

    @Resource
    private IEnrollService enrollService;

    @Resource
    private LoginInfoMapper loginInfoMapper;




    @Override
    @Transactional
    public Object login(LoginDTO dto) {
        int status = 0;
        String token;
        String code = dto.getCode();
        //小程序登录
        Code2SessionResp code2SessionResp = wxAppService.code2Session(code);
        String sessionKey = code2SessionResp.getSession_key();
        //获取openID
        String openid = code2SessionResp.getOpenid();
        //根据opeID判断当前用户是否存在系统中
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getOpenId, openid));
        //不存在当前用户就新增
        if(WLObjectUtils.isEmpty(one)){
            one = new User();
            one.setOpenId(openid);
            userService.save(one);
        }else {
            if(one.getHeadImg() != null){
                status = 1;
            }
        }
        LincolnUser lincolnUser = WLBeanUtils.convert(one, LincolnUser.class);
        lincolnUser.setSessionKey(sessionKey);
        lincolnUser.setOpenId(openid);
        lincolnUser.setId(one.getId());
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setUserId(one.getId());
        loginInfoMapper.insert(loginInfo);
        token = loginService.login(Maps.newHashMap(), openid, lincolnUser);
        HashMap<String, Object> result = new HashMap<>();
        result.put("token",token);
        result.put("status",status);
        return result;
    }

    @Override
    public Object idea(LincolnUser user) {
        return null;
    }

    @Override
    public Object enroll(Enroll enroll, LincolnUser lincolnUser) {
        enroll.setUserId(lincolnUser.getId());
        User user = userService.getById(lincolnUser.getId());
        enroll.setPhone(user.getPhone());
        if ((boolean)isEnroll(lincolnUser)){
            throw new BusinessException(CodeEnum.ENROLL_EXIT);
        }else {
            enrollService.save(enroll);
        }
        return null;
    }

    @Override
    public Object isEnroll(LincolnUser lincolnUser) {
        Integer id = lincolnUser.getId();
        Enroll user = enrollService.getOne(new QueryWrapper<Enroll>().lambda().eq(Enroll::getUserId,id));
        return !WLObjectUtils.isEmpty(user);
    }

    @Override
    public Object getPhoneNumber(String code, LincolnUser user) {
        int nums = 0;
        Integer id = user.getId();
        User users = userService.getById(id);
        users.setPhone(getPhone(nums, code));
        userService.saveOrUpdate(users);
        return null;
    }

    //简单重试
    String getPhone(Integer nums,String code){
        if(nums <= 6){
            PhoneNumberInfoResp phoneInfo = wxAppService.getPhoneNumber(code).getPhone_info();
            if(phoneInfo == null){
                nums = nums+1;
                try {
                    Thread.sleep(200);
                }catch (Exception ignored){

                }
                getPhone(nums,code);
            }else {
                return phoneInfo.getPhoneNumber();
            }
        }
        throw new BusinessException(CodeEnum.GET_PHONE_ERROR);
    }

    @Override
    public Object register(LoginDTO dto, LincolnUser user) {
        dto.setSessionKey(user.getSessionKey());
        //获取用户信息（解密加密数据）
        UserInfoResp userInfo = wxAppService.getUserInfo(WLBeanUtils.convert(dto, UserInfoReq.class));
        User one = new User();
        // TODO: 2022/2/15 获取手机号（等待权限）
        one.setOpenId(user.getOpenId());
        one.setId(user.getId());
        one.setHeadImg(userInfo.getAvatarUrl());
        one.setName(userInfo.getNickName());
        one.setNikeName(userInfo.getNickName());
        userService.updateById(one);
        return null;
    }
}

