package com.wpp.handred.service;

import com.wpp.handred.auth.LincolnUser;
import com.wpp.handred.entity.Enroll;
import com.wpp.handred.entity.dto.LoginDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface AppService {

    Object login(LoginDTO loginDTO);

    Object idea(LincolnUser user) ;


    Object enroll(Enroll enroll,LincolnUser lincolnUser);


    Object isEnroll(LincolnUser lincolnUser);

    Object getPhoneNumber(String code, LincolnUser user);

    Object register(LoginDTO dto, LincolnUser user);
}
