package com.wpp.handred.service.impl;

import com.wpp.handred.entity.User;
import com.wpp.handred.mapper.UserMapper;
import com.wpp.handred.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangle
 * @since 2022-02-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
