package com.wpp.handred.service;

import com.wpp.handred.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangle
 * @since 2022-02-14
 */
public interface IUserService extends IService<User> {



}
