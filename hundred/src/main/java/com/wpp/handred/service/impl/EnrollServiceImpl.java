package com.wpp.handred.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.handred.entity.Enroll;
import com.wpp.handred.mapper.EnrollMapper;
import com.wpp.handred.service.IEnrollService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangle
 * @since 2022-02-15
 */
@Service
public class EnrollServiceImpl extends ServiceImpl<EnrollMapper, Enroll> implements IEnrollService {

}
