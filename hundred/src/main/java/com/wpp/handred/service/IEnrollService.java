package com.wpp.handred.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.handred.entity.Enroll;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangle
 * @since 2022-02-15
 */
public interface IEnrollService extends IService<Enroll> {

}
