package com.wpp.handred.controller;

import com.lexiang.oauth.annotation.CheckUser;
import com.wpp.handred.auth.DataResult;
import com.wpp.handred.auth.LincolnUser;
import com.wpp.handred.entity.Enroll;
import com.wpp.handred.entity.dto.LoginDTO;
import com.wpp.handred.service.AppService;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  application
 * </p>
 *
 * @author wangle
 * @since 2022-02-14
 */
@RestController
@RequestMapping("/hundred/app")
public class AppController {


    @Resource
    private AppService appService;

    @PostMapping("/login")
    public Object login(@RequestBody LoginDTO dto){
       return new DataResult<>().setResult(appService.login(dto)).ok();
    }


    @GetMapping("/getPhoneNumber")
    @CheckUser
    public Object getPhoneNumber(@Param("code") String code,LincolnUser user){
        return new DataResult<>().setResult(appService.getPhoneNumber(code,user)).ok();
    }

    @PostMapping("/register")
    @CheckUser
    public Object register(@RequestBody LoginDTO dto,LincolnUser user) {
        return new DataResult<>().setResult(appService.register(dto,user)).ok();
    }


    @PostMapping("/enroll")
    @CheckUser
    public Object enroll(@RequestBody Enroll enroll,LincolnUser lincolnUser) {
       return new DataResult<>().setResult(appService.enroll(enroll,lincolnUser)).ok();
    }

    @GetMapping("/isEnroll")
    @CheckUser
    public Object isEnroll(LincolnUser user) {
        return new DataResult<>().setResult(appService.isEnroll(user)).ok();
    }


    public static void main(String[] args) throws Exception{
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.yield();

                } catch (Exception e) {

                }
            }
        });
        thread.start();
        Thread.sleep(1000);
        System.out.println(thread.getState());

    }
}
