package com.wpp.handred.entity.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginDTO {

    @ApiModelProperty("登录需要的code")
    private String code;

    @ApiModelProperty("获取用户信息的签名")
    private String signature;

    @ApiModelProperty("获取用户信息的加密数据")
    private String encryptedData;

    @ApiModelProperty("无需传递，防止信息泄漏")
    private String sessionKey;

    @ApiModelProperty("获取用户信息iv")
    private String iv;

}
