package com.wpp.handred.mapper;

import com.wpp.handred.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangle
 * @since 2022-02-14
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
