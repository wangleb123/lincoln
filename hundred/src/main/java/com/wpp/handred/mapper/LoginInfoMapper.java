package com.wpp.handred.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.handred.entity.LoginInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangle
 * @since 2022-02-22
 */
@Mapper
public interface LoginInfoMapper extends BaseMapper<LoginInfo> {

}
