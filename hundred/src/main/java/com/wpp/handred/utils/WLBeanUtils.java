package com.wpp.handred.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.http.ResponseEntity;

public class WLBeanUtils {

    public static  <T> T convert(Object data, Class<T> clazz){
       return JSON.parseObject(JSON.toJSONString(data),clazz);
    }

}



