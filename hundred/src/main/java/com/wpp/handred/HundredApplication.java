package com.wpp.handred;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HundredApplication {

    public static void main(String[] args) {
        SpringApplication.run(HundredApplication.class, args);
    }

}
