package com.wpp.handred.result;


import com.lexiang.oauth.utils.BusinessException;
import com.wpp.handred.auth.DataResult;
import com.wpp.handred.auth.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public Result customException(BusinessException e) {
        DataResult<Object> dataResult = new DataResult<>();
        dataResult.setCode(e.getCode());
        dataResult.setMsg(e.getMessage());
        return dataResult;
    }
}