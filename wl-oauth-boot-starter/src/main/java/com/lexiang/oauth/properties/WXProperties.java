package com.lexiang.oauth.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "wx")
public class WXProperties {

    private String appId;

    private String appSecret;

}
