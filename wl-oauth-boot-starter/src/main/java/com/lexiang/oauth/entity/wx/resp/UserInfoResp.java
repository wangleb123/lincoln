package com.lexiang.oauth.entity.wx.resp;


import com.lexiang.oauth.entity.WxBase;
import lombok.Data;

@Data
public class UserInfoResp extends WxBase {

    private String nickName;

    private Integer gender;

    private String language;

    private String city;

    private String province;

    private String country;

    private String avatarUrl;

}
