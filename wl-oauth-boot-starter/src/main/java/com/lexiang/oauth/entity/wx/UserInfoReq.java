package com.lexiang.oauth.entity.wx;

import lombok.Data;

@Data
public class UserInfoReq {

    private String signature;

    private String encryptedData;

    private String sessionKey;

    private String iv;

}
