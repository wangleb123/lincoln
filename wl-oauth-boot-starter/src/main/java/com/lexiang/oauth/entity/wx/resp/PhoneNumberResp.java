package com.lexiang.oauth.entity.wx.resp;


import com.lexiang.oauth.entity.WxBase;
import lombok.Data;

@Data
public class PhoneNumberResp extends WxBase {

    private PhoneNumberInfoResp phone_info;
}
