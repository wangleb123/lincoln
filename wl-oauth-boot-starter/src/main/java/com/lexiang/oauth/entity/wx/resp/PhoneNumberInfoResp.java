package com.lexiang.oauth.entity.wx.resp;


import lombok.Data;

@Data
public class PhoneNumberInfoResp {

    private String phoneNumber;

    private String purePhoneNumber;

    private String countryCode;

}
