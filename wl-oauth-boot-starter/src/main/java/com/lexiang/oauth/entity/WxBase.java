package com.lexiang.oauth.entity;


import lombok.Data;

@Data
public class WxBase {


    //错误码
    private long errcode;


    //错误信息
    private String errmsg;
}
