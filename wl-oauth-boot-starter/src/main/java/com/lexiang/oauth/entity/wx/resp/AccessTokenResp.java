package com.lexiang.oauth.entity.wx.resp;


import com.lexiang.oauth.entity.WxBase;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccessTokenResp extends WxBase {


    private String access_token;

    private long expires_in;





}
