package com.lexiang.oauth.entity.wx.resp;


import com.lexiang.oauth.entity.WxBase;
import lombok.Data;

@Data
public class Code2SessionResp extends WxBase {


    //用户唯一标识
    private String openid;

    //会话密钥
    private String session_key;

    //用户在开放平台的唯一标识符
    private String unionid;


}
