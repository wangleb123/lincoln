package com.lexiang.oauth.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class TokenUtils {

    public static String getToken(){
        HttpServletRequest request = HttpRequestUtils.getRequest();
        String tokenHead = request.getHeader("token");
        String tokenParam = request.getParameter("token");
        if(tokenHead == null && tokenParam == null){
            log.error("访问时请携带token");
            throw new BusinessException(CodeEnum.NO_TOKEN);
        }else {
            return tokenHead == null ? tokenParam : tokenHead;
        }
    }


}
