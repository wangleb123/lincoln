package com.lexiang.oauth.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @description:
 * @author: wangle
 * @time: 2020/9/28 12:09
 */
public class WLRandomUtils {

    private static Random random = new Random();


    public static int randomInt(){
        return random.nextInt(100);
    }

    public static int randomInt(Integer size){
        return random.nextInt((int)Math.pow(new Integer(10).doubleValue(),size.doubleValue()));
    }

    public static String randomString(int count){
       return RandomStringUtils.randomAlphanumeric(count);
    }



    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(WLRandomUtils.randomString(12));
        }
    }

}
