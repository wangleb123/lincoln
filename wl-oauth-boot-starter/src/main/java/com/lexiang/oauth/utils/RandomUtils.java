package com.lexiang.oauth.utils;

import java.security.SecureRandom;

public class RandomUtils {

    public static  char[] numChars = new char[]{'1','2','3', '4', '5', '6', '7', '8', '9'};

    public static  char[] StringChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'};

    public static  char[] allChars = new char[]{'1','2','3', '4', '5', '6', '7', '8', '9','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'};

    public static int initLength = 7;

    public static String getNumbers(){
        return getNumbers(initLength);
    }

    public static String getString1(){
        return getString1(initLength);
    }

    public static String getString2(){
        return getString2(initLength);
    }


    /**
     *
     * @param length 随机数长度
     * @return 返回质保换数字的字符串，如 "12123e"
     */
    public static String getNumbers(int length){
        return getRandom(length,numChars);
    }

    /**
     *
     * @param length 随机数长度
     * @return 只返回数字之外的字符串，如 "bjijjkvl"
     */
    public static String getString1(int length){
        return getRandom(length,StringChars);
    }

    /**
     *
     * @param length 随机数长度
     * @return 返回含数字的字符串如 "bjijj123123kvl"
     */
    public static String getString2(int length){
        return getRandom(length,allChars);
    }

    private static String getRandom(int length,char[] chars){
        StringBuilder sb = new StringBuilder();
        SecureRandom secureRandom = new SecureRandom();

        for (int i = 0; i < length; i++) {
            int j = secureRandom.nextInt(chars.length);
            sb.append(chars[j]);
        }
        return sb.toString();
    }

}






