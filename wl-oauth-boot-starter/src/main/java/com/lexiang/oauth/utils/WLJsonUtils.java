package com.lexiang.oauth.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.gson.GsonBuilder;

import java.security.MessageDigest;
import java.util.Map;

public class WLJsonUtils {

    static GsonBuilder gb = new GsonBuilder();
    /**
     *
     * @param map 传入map
     * @param clazz 传入class类型
     * @param <T> 范型
     * @return 传入class类型对应的对象实例
     */
    public static <T> T MapToObject(Map<String,Object> map, Class<T> clazz){
        if(map != null){
            return JSON.parseObject(JSON.toJSONString(map),clazz);
        }
        return null;
    }

    public static  <T> T ObjectToBean(Object object,Class<T> clazz){
        if(object != null){
            String s = object.toString();
            return JSONObject.parseObject(JSON.toJSONString(JSONObject.parse(s)), clazz);
        }
        return null;
    }

    /**
     * 将对象装换为map
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, T> beanToMap(Object bean) {
        Map<String ,T> map = JSON.parseObject(JSON.toJSONString(bean, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteMapNullValue), Map.class);
        return map;
    }


    public static String objectToJson(Object object){
       return gb.create().toJson(object);
    }

    public static <T> T jsonToobject(String json ,Class<T> clazz){
       return JSONObject.parseObject(json,clazz);
    }

    public static void main(String[] args) throws Exception {

        MessageDigest digest = MessageDigest.getInstance("SHA-1");

        System.out.println(Base64Utils.encode(digest.digest("wangsaichao:123456".getBytes())));
    }
}
