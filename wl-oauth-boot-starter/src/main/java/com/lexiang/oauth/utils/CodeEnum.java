package com.lexiang.oauth.utils;

public enum CodeEnum {

    /**
     * 错误类型
     */
    NO_TOKEN(1L,"请传入token"),

    USER_NOT_LOGIN(2L,"用户未登录"),


    CODE_SESSION_ERROR(3L,"微信小程序登录失败"),


    result_not_exist(4L,"记录不存在"),
    ENROLL_EXIT(5L,"您已经报名，请勿重复提交！"),

    GET_PHONE_ERROR(6L,"获取手机号码失败"),



            ;

    /**
     * 错误码
     */
    private Long code;

    /**
     * 提示信息
     */
    private String msg;


    CodeEnum(Long code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public Long getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
