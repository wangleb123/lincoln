package com.lexiang.oauth;

import com.lexiang.oauth.properties.LoginProperties;
import com.lexiang.oauth.properties.WXProperties;
import com.lexiang.oauth.service.LoginService;
import com.lexiang.oauth.service.RedisService;
import com.lexiang.oauth.service.WxAppService;
import com.lexiang.oauth.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisServer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Configuration
@EnableConfigurationProperties({LoginProperties.class, WXProperties.class})
@Import({RedisService.class,JwtUtils.class})
public class DefaultAutoConfiguration {

    @Resource
    private LoginProperties loginProperties;

    @Resource
    public RedisService redisServer;

    @Resource
    private JwtUtils jwtUtils;

    @Bean
    public LoginService loginService(){
        return new LoginService(jwtUtils,redisServer,loginProperties);
    }

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new WxMappingJackson2HttpMessageConverter());
        return restTemplate;
    }

    @Bean
    public WxAppService wxAppService(){
        return new WxAppService();
    }


}
