#设置镜像使用的基础镜像 这里使用的是oracle jdk8
FROM adoptopenjdk/openjdk8
#设置镜像暴露的端口 这里要与application.properties中的server.port保持一致
EXPOSE 3389
#设置容器的挂载卷
VOLUME /tmp
ARG profile
ENV PRO=$profile
#编译镜像时将springboot生成的jar文件复制到镜像中
ADD /hundred/target/hundred-0.0.6-SNAPSHOT.jar  /app.jar
#编译镜像时运行脚本
RUN bash -c 'touch /app.jar'
#容器的入口程序，这里注意如果要指定外部配置文件需要使用-spring.config.location指定配置文件存放目录
CMD nohup java -jar /app.jar --spring.profiles.active=$PRO
